package com.example.contract;

import com.bitrisk.exposure.ExposureContract;
import com.bitrisk.exposure.ExposureState;
import com.bitrisk.model.Exposure;
import net.corda.core.crypto.CompositeKey;
import org.junit.Ignore;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.Instant;

import static net.corda.core.utilities.TestConstants.getTEST_TX_TIME;
import static net.corda.testing.CoreTestUtils.*;

public class ExposureTests {

    @Test
    @Ignore
    public void exposureMustBePossitive() {

        final Exposure exposure = new Exposure(Instant.now(), new BigDecimal(0.00), false);

        ledger(ledgerDSL -> {

            ledgerDSL.transaction(txDSL -> {

                txDSL.output(new ExposureState(exposure, getMINI_CORP(), getMEGA_CORP(), new ExposureContract()));

                CompositeKey[] keys = new CompositeKey[2];
                keys[0] = getMEGA_CORP_PUBKEY();
                keys[1] = getMINI_CORP_PUBKEY();

                txDSL.command(keys, ExposureContract.Commands.AddExposure::new);

                txDSL.failsWith("Exposure can not be zero.");
                txDSL.timestamp(getTEST_TX_TIME());
                txDSL.verifies();

                return null;
            });

            return null;
        });
    }
}
