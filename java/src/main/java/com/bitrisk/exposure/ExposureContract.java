package com.bitrisk.exposure;

import net.corda.core.Utils;
import net.corda.core.contracts.*;
import net.corda.core.contracts.clauses.*;
import net.corda.core.crypto.SecureHash;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toList;
import static kotlin.collections.CollectionsKt.single;
import static net.corda.core.contracts.ContractsDSL.requireSingleCommand;

public class ExposureContract implements Contract {

    private final SecureHash legalContractReference = SecureHash.sha256("blah blah blah");

    @Override public final SecureHash getLegalContractReference() { return legalContractReference; }

    private List<AuthenticatedObject<ExposureContract.Commands>> extractCommands(TransactionForContract tx) {
        return tx.getCommands()
                .stream()
                .filter(command -> command.getValue() instanceof ExposureContract.Commands)
                .map(command -> new AuthenticatedObject<>(
                        command.getSigners(),
                        command.getSigningParties(),
                        (ExposureContract.Commands) command.getValue()))
                .collect(toList());
    }

    /**
     * The AllComposition() clause mandates that all specified clauses clauses (in this case [Timestamped] and [Group])
     * must be executed and valid for a transaction involving this type of contract to be valid.
     */
    @Override
    public void verify(TransactionForContract tx) {
        ClauseVerifier.verifyClause(
                tx,
                new AllComposition<>(new ExposureContract.Clauses.Group()),
                extractCommands(tx));
    }

    /**
     * Currently this contract only implements one command. If you wish to add further commands to perhaps Amend() or
     * Cancel() a purchase order, you would add them here. You would then need to add associated clauses to handle
     * transaction verification for the new commands.
     */
    public interface Commands extends CommandData {
        class AddExposure implements IssueCommand, ExposureContract.Commands {
            private final long nonce = Utils.random63BitValue();
            @Override public long getNonce() { return nonce; }
        }
    }

    /**
     * This is where we implement our clauses.
     */
    public interface Clauses {

        // If you add additional clauses, make sure to reference them within the 'AnyComposition()' clause.
        class Group extends GroupClauseVerifier<ExposureState, ExposureContract.Commands, UniqueIdentifier> {
            public Group() { super(new AnyComposition<>(new ExposureContract.Clauses.AddExposure())); }

            @Override public List<TransactionForContract.InOutGroup<ExposureState, UniqueIdentifier>> groupStates(TransactionForContract tx) {
                // Group by purchase order linearId for in/out states.
                return tx.groupStates(ExposureState.class, ExposureState::getLinearId);
            }
        }

        /**
         * Checks various requirements for the placement of a purchase order.
         */
        class AddExposure extends Clause<ExposureState, ExposureContract.Commands, UniqueIdentifier> {

            @Override public Set<ExposureContract.Commands> verify(TransactionForContract tx,
                                                                        List<? extends ExposureState> inputs,
                                                                        List<? extends ExposureState> outputs,
                                                                        List<? extends AuthenticatedObject<? extends ExposureContract.Commands>> commands,
                                                                        UniqueIdentifier groupingKey)
            {
                final AuthenticatedObject<ExposureContract.Commands.AddExposure> command = requireSingleCommand(tx.getCommands(), ExposureContract.Commands.AddExposure.class);
                final ExposureState out = single(outputs);

                /*
                requireThat(require -> {
                    // Generic constraints around generation of the issue purchase order transaction.
                    require.by("No inputs should be consumed when adding an exposure.",
                            inputs.isEmpty());

                    require.by("Only one output state should be created for each group.",
                            outputs.size() == 1);

                    require.by("The buyer and the seller cannot be the same entity.",
                            out.getCounterparty1() != out.getCounterparty2());

                    require.by("All of the participants must be signers.",
                            command.getSigners().containsAll(out.getParticipants()));

                    // Exposure specific constraints.
                    require.by("Exposure can not be zero.",
                            out.getExposure().getValue().longValue() > 0);

                    return null;
                });
*/
                return Collections.singleton(command.getValue());            }
        }
    }
}