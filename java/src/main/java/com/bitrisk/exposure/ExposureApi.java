package com.bitrisk.exposure;

import com.bitrisk.exposure.ExposureContract;
import com.bitrisk.exposure.ExposureState;
import com.example.flow.ExampleFlow;
import com.bitrisk.model.Exposure;
import net.corda.core.contracts.ContractState;
import net.corda.core.contracts.StateAndRef;
import net.corda.core.crypto.Party;
import net.corda.core.messaging.CordaRPCOps;

import java.math.BigDecimal;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import static java.util.Collections.singletonMap;
import static java.util.stream.Collectors.toList;

// This API is accessible from /api/exposure. All paths specified below are relative to it.
@Path("exposure")
public class ExposureApi {

    private final CordaRPCOps services;
    private final String myLegalName;

    public ExposureApi(CordaRPCOps services) {
        this.services = services;
        this.myLegalName = services.nodeIdentity().getLegalIdentity().getName();
    }

    /**
     * Returns the party name of the node providing this end-point.
     */
    @GET
    @Path("me")
    @Produces(MediaType.APPLICATION_JSON)
    public Map<String, String> whoami() { return singletonMap("me", myLegalName); }

    /**
     * Returns all parties registered with the [NetworkMapService]. The names can be used to look up identities by
     * using the [IdentityService].
     */
    @GET
    @Path("peers")
    @Produces(MediaType.APPLICATION_JSON)
    public Map<String, List<String>> getPeers() {
        final String NOTARY_NAME = "Controller";
        return singletonMap(
                "peers",
                services.networkMapUpdates().getFirst()
                        .stream()
                        .map(node -> node.getLegalIdentity().getName())
                        .filter(name -> !name.equals(myLegalName) && !name.equals(NOTARY_NAME))
                        .collect(toList()));
    }

    /**
     * Displays all purchase order states that exist in the vault.
     */
    @GET
    @Path("exposures")
    @Produces(MediaType.APPLICATION_JSON)
    public List<StateAndRef<ContractState>> getExposures() {
        return services.vaultAndUpdates().getFirst();
    }

    @GET
    @Path("postTrade")
    public Response postTrade(
            @QueryParam("tradeType") String tradeType,
            @QueryParam("buySell") String buySell,
            @QueryParam("counterparty") String counterparty,
            @QueryParam("amount") String amountText) throws InterruptedException, ExecutionException {
        System.err.println("postTrade(" + tradeType + ", " + buySell + ", " + counterparty + ", " + amountText + ")");
        BigDecimal pay = BigDecimal.ZERO;
        BigDecimal receive = BigDecimal.ZERO;
        if("BUY".equalsIgnoreCase(buySell)) {
            receive = parseAmount(amountText);
        } else {
            pay = parseAmount(amountText);
        }
        Exposure exposure = new Exposure(counterparty, pay, receive);
        createExposure(exposure, counterparty);
        return Response.status(Response.Status.CREATED).build();
    }

    private BigDecimal parseAmount(String text) {
        try {
            return new BigDecimal(text);
        } catch(Exception x) {
            return BigDecimal.ZERO;
        }
    }

    /**
     * This should only be called from the 'buyer' node. It initiates a flow to agree a purchase order with a
     * seller. Once the flow finishes it will have written the purchase order to ledger. Both the buyer and the
     * seller will be able to see it when calling /api/example/purchase-orders on their respective nodes.
     *
     * This end-point takes a Party name parameter as part of the path. If the serving node can't find the other party
     * in its network map cache, it will return an HTTP bad request.
     *
     * The flow is invoked asynchronously. It returns a future when the flow's call() method returns.
     */
    @PUT
    @Path("{party}/create-exposure")
    public Response createExposure(Exposure exposure, @PathParam("party") String partyName) throws InterruptedException, ExecutionException {
        final Party otherParty = services.partyFromName(partyName);

        if (otherParty == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        final ExposureState state = new ExposureState(
                exposure,
                services.nodeIdentity().getLegalIdentity(),
                otherParty,
                new ExposureContract());

        // The line below blocks and waits for the flow to return.
        final ExposureFlow.ExposureFlowResult result = services
                .startFlowDynamic(ExposureFlow.Initiator.class, state, otherParty)
                .getReturnValue()
                .toBlocking()
                .first();


        final Response.Status status;
        if (result instanceof ExposureFlow.ExposureFlowResult.Success) {
            status = Response.Status.CREATED;
        } else {
            status = Response.Status.BAD_REQUEST;
        }

        return Response
                .status(status)
                .entity(result.toString())
                .build();
    }
}