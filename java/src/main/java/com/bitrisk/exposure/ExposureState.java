package com.bitrisk.exposure;

import static java.util.stream.Collectors.toList;

import java.security.PublicKey;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import com.bitrisk.model.Exposure;

import net.corda.core.contracts.Command;
import net.corda.core.contracts.DealState;
import net.corda.core.contracts.TransactionType;
import net.corda.core.contracts.UniqueIdentifier;
import net.corda.core.crypto.CompositeKey;
import net.corda.core.crypto.Party;
import net.corda.core.transactions.TransactionBuilder;

public class ExposureState implements DealState {

    private final Exposure exposure;
    private final Party counterparty1;
    private final Party counterparty2;

    private final ExposureContract contract;
    private final UniqueIdentifier linearId;

    public ExposureState(Exposure exposure,
                              Party counterparty1,
                              Party counterparty2,
                              ExposureContract contract)
    {
        this.exposure = exposure;

        this.counterparty1 = counterparty1;
        this.counterparty2 = counterparty2;
        this.contract = contract;

        this.linearId = new UniqueIdentifier(
                Integer.toString(1),
                UUID.randomUUID());
    }

    public Exposure getExposure() { return exposure; }
    public Party getCounterparty1() { return counterparty1; }
    public Party getCounterparty2() { return counterparty2; }

    @Override public ExposureContract getContract() { return contract; }
    @Override public UniqueIdentifier getLinearId() { return linearId; }
    @Override public String getRef() { return linearId.getExternalId(); }
    @Override public List<Party> getParties() { return Arrays.asList(counterparty1, counterparty2); }
    @Override public List<CompositeKey> getParticipants() {
        return getParties()
                .stream()
                .map(Party::getOwningKey)
                .collect(toList());
    }

    /**
     * This returns true if the state should be tracked by the vault of a particular node. In this case the logic is
     * simple; track this state if we are one of the involved parties.
     */
    @Override public boolean isRelevant(Set<? extends PublicKey> ourKeys) {
        final List<PublicKey> partyKeys = getParties()
                .stream()
                .flatMap(party -> party.getOwningKey().getKeys().stream())
                .collect(toList());
        return ourKeys
                .stream()
                .anyMatch(partyKeys::contains);

    }

    /**
     * Helper function to generate a new Issue() purchase order transaction. For more details on building transactions
     * see the API for [TransactionBuilder] in the JavaDocs.
     * https://docs.corda.net/api/net.corda.core.transactions/-transaction-builder/index.html
     */
    @Override public TransactionBuilder generateAgreement(Party notary) {
        return new TransactionType.General.Builder(notary)
                .withItems(this, new Command(new ExposureContract.Commands.AddExposure(), getParticipants()));
    }
}