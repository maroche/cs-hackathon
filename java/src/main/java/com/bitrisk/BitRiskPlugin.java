package com.bitrisk;

import com.esotericsoftware.kryo.Kryo;
import com.bitrisk.exposure.ExposureApi;
import com.bitrisk.exposure.ExposureContract;
import com.bitrisk.exposure.ExposureState;
import com.bitrisk.exposure.ExposureFlow;
import com.bitrisk.model.Exposure;
import com.bitrisk.model.BuySell;
import com.bitrisk.model.Region;
import com.bitrisk.model.Counterparty;
import com.bitrisk.model.SpotTrade;
import com.bitrisk.exposure.ExposureService;
import net.corda.core.crypto.Party;
import net.corda.core.flows.IllegalFlowLogicException;
import net.corda.core.messaging.CordaRPCOps;
import net.corda.core.node.CordaPluginRegistry;
import net.corda.core.node.PluginServiceHub;

import java.time.Instant;
import java.util.*;
import java.util.function.Function;

public class BitRiskPlugin extends CordaPluginRegistry {
    /**
     * A list of classes that expose web APIs.
     */
    private final List<Function<CordaRPCOps, ?>> webApis = Collections.singletonList(ExposureApi::new);

    /**
     * A list of flows required for this CorDapp. Any flow which is invoked from from the web API needs to be
     * registered as an entry into this map. The map takes the form:
     *
     * Name of the flow to be invoked -> Set of the parameter types passed into the flow.
     *
     * E.g. In the case of this CorDapp:
     *
     * "ExposureFlow.Initiator" -> Set(PurchaseOrderState, Party)
     *
     * This map also acts as a white list. If a flow is invoked via the API and not registered correctly
     * here, then the flow state machine will _not_ invoke the flow. Instead, an exception will be raised.
     */
    private final Map<String, Set<String>> requiredFlows = Collections.singletonMap(
            ExposureFlow.Initiator.class.getName(),
            new HashSet<>(Arrays.asList(
                    ExposureState.class.getName(),
                    Party.class.getName()
            )));

    /**
     * A list of long lived services to be hosted within the node. Typically you would use these to register flow
     * factories that would be used when an initiating party attempts to communicate with our node using a particular
     * flow. See the [ExposureService.Service] class for an implementation.
     */
    private final List<Function<PluginServiceHub, ?>> servicePlugins = Collections.singletonList(ExposureService::new);

    /**
     * A list of directories in the resources directory that will be served by Jetty under /web.
     */
    private final Map<String, String> staticServeDirs = Collections.singletonMap(
            // This will serve the exampleWeb directory in resources to /web/example
            "bitrisk", getClass().getClassLoader().getResource("bitrisk").toExternalForm()
    );

    @Override public List<Function<CordaRPCOps, ?>> getWebApis() { return webApis; }
    @Override public Map<String, Set<String>> getRequiredFlows() { return requiredFlows; }
    @Override public List<Function<PluginServiceHub, ?>> getServicePlugins() { return servicePlugins; }
    @Override public Map<String, String> getStaticServeDirs() { return staticServeDirs; }

    /**
     * Register required types with Kryo (our serialisation framework).
     */
    @Override public boolean registerRPCKryoTypes(Kryo kryo) {
        kryo.register(ExposureState.class);
        kryo.register(ExposureContract.class);
        kryo.register(Exposure.class);
        kryo.register(Counterparty.class);
        kryo.register(SpotTrade.class);
        kryo.register(BuySell.class);
        kryo.register(Region.class);
        kryo.register(Instant.class);
        kryo.register(ExposureFlow.ExposureFlowResult.Success.class);
        kryo.register(ExposureFlow.ExposureFlowResult.Failure.class);
        kryo.register(IllegalArgumentException.class);
        kryo.register(IllegalFlowLogicException.class);
        return true;
    }
}