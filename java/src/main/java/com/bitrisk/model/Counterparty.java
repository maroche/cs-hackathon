package com.bitrisk.model;

public class Counterparty {

    private String name;
    private Region region;

    public Counterparty(String name, Region region) {
        this.name = name;
        this.region = region;
    }

    public String getName() {
        return name;
    }

    public Region getRegion() {
        return region;
    }

    @Override
    public String toString() {
        return String.format("Counterparty(name=%s, region=%s)", name, region);
    }

}
