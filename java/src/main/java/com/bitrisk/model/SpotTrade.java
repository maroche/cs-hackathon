package com.bitrisk.model;

import java.math.BigDecimal;

public class SpotTrade {

    private BuySell buySell;
    private Counterparty counterparty;
    private BigDecimal amount;

    public SpotTrade(BuySell bs, Counterparty  counterparty, BigDecimal amount) {
        this.buySell = bs;
        this.counterparty = counterparty;
        this.amount = amount;
    }

    public BuySell getBuySell() {
        return buySell;
    }

    public Counterparty getCounterparty() {
        return counterparty;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return String.format("SpotTrade(%s, cpty=%s, amount=%2.0f)", buySell, counterparty, amount);
    }

}
