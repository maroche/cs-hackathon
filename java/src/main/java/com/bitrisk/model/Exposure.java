package com.bitrisk.model;

import java.math.BigDecimal;
import java.time.Instant;

public class Exposure {

    private Instant date;
    private String counterparty;
    private BigDecimal payAmount;
    private BigDecimal receiveAmount;
    private boolean swiss;

    public String getCounterparty() { return counterparty; }
    public BigDecimal getPayAmount() { return payAmount; }
    public BigDecimal getReceiveAmount() { return receiveAmount; }

    public boolean isSwiss() { return swiss; }

    public Exposure(String counterparty, BigDecimal pay, BigDecimal receive) {
        this.counterparty = counterparty;
        this.payAmount = pay;
        this.receiveAmount = receive;
    }

    // Dummy constructor used by the createPurchaseOrder API endpoint.
    public Exposure() {}

    @Override public String toString() {
        return String.format("Exposure(cpty=%s, pay=%0.2f, receive=%0.2f)",
                counterparty,
                payAmount,
                receiveAmount);
    }
}