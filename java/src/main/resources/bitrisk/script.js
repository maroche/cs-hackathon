// JavaScript Document
var getJSON = function(url, successHandler, errorHandler) {
	var xhr = typeof XMLHttpRequest != 'undefined'
		? new XMLHttpRequest()
		: new ActiveXObject('Microsoft.XMLHTTP');
	xhr.open('get', url, true);
	xhr.responseType = 'json';
	xhr.onreadystatechange = function() {
		var status;
		var data;
		// https://xhr.spec.whatwg.org/#dom-xmlhttprequest-readystate
		if (xhr.readyState == 4) { // `DONE`
			status = xhr.status;
			if (status == 200) {
				successHandler && successHandler(xhr.response);
			} else {
				errorHandler && errorHandler(status);
			}
		}
	};
	xhr.send();
};

function adjustRisk(source) {
	var node = findByName(source.parentElement.parentElement.childNodes, "result")
	var text = findByName(node.childNodes, "text");
	var opts = findByName(node.childNodes, "options");
	if(source.value == "adjust") {
		source.value = "submit"
		text.style.display = "none";
		opts.style.display = "";
	} else {
		source.value = "adjust"
		text.style.display = "";
		opts.style.display = "none";
	}
}

function postTrade(form) {
    form.submit();
    alert("Trade entered into ledger");
}

function refreshExposures() {
    var tbl = document.all.exposures;
    for(var i = tbl.rows.length; --i > 0; i++) {
        tbl.deleteRow(i);
    }

    var exposures = getJSON('/api/exposure/exposures', function(data) {
        alert("refresh completed: " + data);
    }, function(status) {
        alert("failed to load data");
    })
}

function findByName(children, name) {
	for(var i = 0; i < children.length; i++) {
		if(children[i].id == name) return children[i];
	}
	return nulll;
}