<!DOCTYPE HTML>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" href="../style.css" type="text/css" />
<title>bit-risk: risk</title>
<script type="text/javascript" src="/web/bitrisk/script.js"></script>
</head>

<body id="RiskPage" style="height:100%">
<table class="page" style="width:100%; height:100%" border="0" cellspacing="5pt" cellpadding="0">
	<tr valign="top">
    	<td>
        	<div class="titlePanel" style="height:70pt">
          		<center><img id="title" src="../images/title.png" alt="Bit-Risk" /></center>
                <div class="tab" valign="top"><span class="tabText">Risk Engine</span></div>
        	</div>
      	</td>
    </tr>
    <tr height="100%" valign="top">
    	<td class="panel">
            <p class="details">
            	Internal systems can use exposure to calculate risk.<br />
                Adjustments to risk should be carefully tracked.<br />
            </p>
            <table class="formTable" border="1" cellspacing="0" cellpadding="5" style="margin-left: 50pt; margin-top: 5pt">
                <th>Counterparty</th>
                <th>Test-Type</th>
                <th>Pass/Fail</th>
                <th>Actions</th>
                <tr>
                	<td>HSBC</td>
                    <td>Stress-Test</td>
                    <td id="result">
                    	<div id="text">Fail</div>
                        <div id="options" style="display:none">
                        	<select>
                            	<option>Pass</option>
                                <option selected="selected">Fail</option>
                            </select>
                        </div>
                    </td>
					<td><input type="button" value="adjust" onclick="adjustRisk(this)" /></td>
                </tr>
			</table>
            <table class="actionBar"  style="margin-left: 50pt">
            	<tr>
                	<td><input class="actionButton" type="button" value="recalc" src="transparent.png" /></td>
                </tr>
            </table>
        </td>
    </tr>
</body>
</table>
</html>
