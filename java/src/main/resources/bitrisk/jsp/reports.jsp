<!DOCTYPE HTML>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" href="../style.css" type="text/css" />
<title>bit-risk: reports</title>
</head>

<body id="ReportsPage" style="height:100%">
<table class="page" style="width:100%; height:100%" border="0" cellspacing="5pt" cellpadding="0">
	<tr valign="top">
    	<td>
        	<div class="titlePanel" style="height:70pt">
          		<center><img id="title" src="../images/title.png" alt="Bit-Risk" /></center>
                <div class="tab" valign="top"><span class="tabText">Report Manager</span></div>
        	</div>
      	</td>
    </tr>
    <tr height="100%" valign="top">
    	<td class="panel">
            <p class="details">
				A manager at Credit-Suisse can use exposure and risk to create reports.<br />
                Reports may be shared with many regulators.<br />
                Different regulators may see a different reports - like Swiss regulator.
            </p>
            <table class="formTable" border="1" cellspacing="0" cellpadding="5" style="margin-left: 50pt; margin-top: 5pt">
                <th>Report</th>
                <th>Counterparty</th>
                <th>Exposure</th>
                <th>Risk</th>
                <th>Actions</th>
                <tr>
                	<td>Stress-Test</td>
                    <td>HSBC</td>
                    <td>1</td>
                    <td>Pass</td>
                    <td><input type="button" value="submit" /></td>
                </tr>
			</table>
            <table class="actionBar"  style="margin-left: 50pt">
            	<tr>
                	<td><input class="actionButton" type="button" value="create-report" src="transparent.png" /></td>
                </tr>
            </table>
        </td>
    </tr>
</body>
</table>
</html>
