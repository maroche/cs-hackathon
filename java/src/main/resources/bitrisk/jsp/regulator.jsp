<!DOCTYPE HTML>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" href="../style.css" type="text/css" />
<title>bit-risk: regulator</title>
</head>

<body id="RegulatorPage" style="height:100%">
<table class="page" style="width:100%; height:100%" border="0" cellspacing="5pt" cellpadding="0">
	<tr valign="top">
    	<td>
        	<div class="titlePanel" style="height:70pt">
          		<center><img id="title" src="../images/title.png" alt="Bit-Risk" /></center>
                <div class="tab" valign="top"><span class="tabText">Regulator View</span></div>
        	</div>
      	</td>
    </tr>
    <tr height="100%" valign="top">
    	<td class="panel">
            <p class="details">
            	A regulator can see reports submitted by banks.<br />
                It can accept or reject them.<br />
                This may be shared with other regulators/institutions.<br />
			</p>
            <table class="formTable" border="1" cellspacing="0" cellpadding="5" style="margin-left: 50pt; margin-top: 5pt">
                <th>From</th>
                <th>Counterparty</th>
                <th>Exposure</th>
                <th>Stress-Test</th>
                <th>Actions</th>
                <tr>
                	<td>Credit-Suisse</td>
                    <td>HSBC</td>
                    <td>1000</td>
                    <td>Pass</td>
                    <td>
                    	<input type="button" value="accept" />
                        <input type="button" value="reject" />
                    </td>
                </tr>
			</table>
            <table class="actionBar"  style="margin-left: 50pt">
            	<tr>
                	<td><input class="actionButton" type="button" value="refresh" src="transparent.png" /></td>
                </tr>
            </table>
        </td>
    </tr>
</body>
</table>
</html>
