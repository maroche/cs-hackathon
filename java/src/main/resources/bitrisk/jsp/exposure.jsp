<!DOCTYPE HTML>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" href="../style.css" type="text/css" />
<title>bit risk: exposures</title>
<script type="text/javascript" src="../script.js"></script>
</head>

<body id="ExposurePage" style="height:100%">
<table class="page" style="width:100%; height:100%" border="0" cellspacing="5pt" cellpadding="0">
	<tr valign="top">
    	<td>
        	<div class="titlePanel">
          		<center><img id="title" src="../images/title.png" alt="Bit-Risk" /></center>
                <div class="tab" valign="top"><span class="tabText">Exposure View</span></div>
        	</div>
      	</td>
    </tr>
    <tr height="20%" valign="top">
    	<td class="panel">
            <p class="details">
           		When a trade is confirmed it gets entered into Corda.<br />
                This could be done automatically for electronic trading.<br />
                This causes "exposure state" to be updated.
                Exposure-State between counterparties can be shared.
            </p>

            <form method="get" action="/api/exposure/postTrade" target="hidden" onsubmit="postTrade(this)">
            <table class="formTable" border="1" cellspacing="0" cellpadding="5" style="margin-left: 50pt; margin-top: 5pt">
                <th>Type</th>
                <th>Buy/Sell</th>
                <th>Counterparty</th>
                <th>Amount</th>
                <tr>
                    <td>
                        <select name="tradeType">
                            <option>FX Spot</option>
                        </select>
                    </td>
                    <td>
                    	<select name="buySell">
                        	<option>Buy</option>
                            <option>Sell</option>
                        </select>
                    </td>
                    <td>
                        <select name="counterparty">
                            <option>Barclays</option>
                            <option>HSBC</option>
                            <option>Lehman Brothers</option>
                        </select>
                    </td>
                    <td>
                        <input name="amount" type="text" size="20" value="1000000" />
                    </td>
                </tr>
            </table>

            <table class="actionBar" style="margin-left: 50pt">
            	<tr>
                	<td><input class="actionButton" type="submit" value="enter" src="transparent.png" /></td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
    <tr height="80%" valign="top">
    	<td class="panel">
            <p class="details">
           		We can then track the exposures between us and all counterparties by viewing our ledger.
            </p>
            <table id="exposures" class="formTable" border="1" cellspacing="0" cellpadding="5" style="margin-left: 50pt; margin-top: 5pt">
                <th>Counterparty</th>
                <th>Pay Amount</th>
                <th>Receive Amount</th>
                <th>Net Amount</th>
			</table>
            <table class="actionBar"  style="margin-left: 50pt">
            	<tr>
                	<td>
                	    <input class="actionButton" type="button" value="refresh" src="transparent.png" onclick="refreshExposures()" />
                        <iframe name="hidden" style="display:none;"></iframe>
                	</td>
                </tr>
            </table>
        </td>
    </tr>
</body>
</table>
</html>
